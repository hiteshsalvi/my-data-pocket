import json
import argparse
from datetime import datetime
import os
from pytz import timezone
import subprocess


deploy_command = f'gcloud --quiet --project { os.environ["PROJECT_ID"] } app deploy app.yaml'

def execute_command(command):
  execute = subprocess.Popen(command.split())
  execute.wait()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("env", help="Possible values [ dev | prod ]")
  args = parser.parse_args()
  print(args.env)
  if args.env == 'dev' or args.env == 'development':
    # Deploy
    deploy_command = f'{deploy_command} -v dev --no-promote'
    execute_command(deploy_command) # execute
  elif args.env == 'prod' or args.env == 'production':
    # Deploy
    deploy_command = f'{deploy_command} -v prod-{ datetime.now(tz=timezone("Asia/Calcutta")).strftime("%H-%M-%S-%d-%m-%y") }'
    execute_command(deploy_command) # execute