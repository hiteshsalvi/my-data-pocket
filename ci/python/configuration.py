import subprocess


configuration_command = f'npm start'

def execute_command(command):
  execute = subprocess.Popen(command.split())
  execute.wait()


if __name__ == '__main__':
  # Build
  execute_command('pwd')
  execute_command(configuration_command)  # execute

