import json
import argparse
from datetime import datetime
import os
import subprocess


# build_command = f'npm run-script build --prefix ci/nodejs'
build_command = 'gulp build'

def execute_command(command):
  execute = subprocess.Popen(command.split())
  execute.wait()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("env", help="Possible values [ dev | prod ]")
  args = parser.parse_args()
  print(args.env)
  build_command = build_command if args.env == 'prod' else f'{build_command}'  # '--dev'
  execute_command(build_command)  # execute

