const { google } = require('googleapis');
let fs = require('fs');
const resolve = require('path').resolve;
const serviceAccount = require(`/tmp/${process.env.CI_PIPELINE_ID}.json`);

const drive = google.drive({version: 'v3'});

const scopes = [
  'https://www.googleapis.com/auth/drive'
];

const driveServiceUser = 'ci-cd-at-my-data-pocket@my-data-pocket.iam.gserviceaccount.com';

const jWTClient = new google.auth.JWT(
  serviceAccount.client_email,
  null,
  serviceAccount.private_key,
  scopes,
  driveServiceUser
);

jWTClient.authorize(res => {});
async function main(fileName){

  async function list() {
    return new Promise(function(resolve, reject) {
      drive.files.list({
        auth: jWTClient,
        q: `name="${fileName}"`,
        fields: 'files(id, name)',
        spaces: 'drive'
      },
      function (error, file) {
        if (file) {
          resolve(file.data.files);
        }
        if (error){
          console.error(error);
          reject(error);
        }
      });
    });
  }

  async function download(l){
    var dest = resolve(`../../configs/${l[0].name}`);
    console.log(`downloading configuration and saving at ${dest}`);
    dest = fs.createWriteStream(resolve(dest));
    return new Promise(function(resolve, reject) {
      drive.files.get({
        auth: jWTClient,
        fileId: l[0].id,
        alt: 'media'
      }, {
        responseType: 'stream'  // 'arraybuffer' | 'blob' | 'json' | 'text' | 'stream'
      },
      function (error, file) {
        if (error){
          reject(error);
          console.error(error);
        }
        file.data
          .on('end', () => {
          })
          .on('error', error => {
              console.error(error);
              process.exit();
          })
          .pipe(dest);
          resolve(dest);
        });
      });
}

  let l = await list();
  console.log(l);
  let d = await download(l);
}

main('env-config.json')
  .then()
  .catch(error => {
    console.error(error);
  });