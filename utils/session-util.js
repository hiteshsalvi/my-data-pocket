function getTwoFactorAuthenticated(req, res, next) {
  if (req.user && req.session.user) {
      next();
  } else {
      res.redirect('/');
  }
}

function getUserAuthenticated(req, res, next) {
  console.log(req.user);
  if (req.user) {
      next();
  } else {
      res.redirect('/');
  }
}

module.exports = {
  getTwoFactorAuthenticated: getTwoFactorAuthenticated,
  getUserAuthenticated: getUserAuthenticated
}