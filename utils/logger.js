const winston = require('winston');

// Imports the Google Cloud client library for Winston
const { LoggingWinston } = require('@google-cloud/logging-winston');

const loggingWinston = new LoggingWinston({logName: `gae-my_data_pocket-${process.env.NODE_ENV}`});

// Create a Winston logger that streams to Stackdriver Logging
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/winston_log"

let logger = winston.createLogger({
  level: 'info',
  transports: [
    new winston.transports.Console(),
    // Add Stackdriver Logging
    loggingWinston,
  ],
});

module.exports = {
    log: logger.info.bind(logger),
    error: logger.error.bind(logger),
    debug: logger.debug.bind(logger),
    // critical: logger.crit.bind(logger),
    // warning: logger.warning.bind(logger)
  };