// 'use strict';

const crypto = require('crypto');
const encryptionKey = require('../configs/env-config.json')[
  process.env.NODE_ENV ? process.env.NODE_ENV : 'development'
];

function encrypt(data) {
  return crypto.createHash("sha256")
    .update(`${data.password.slice(0, 2)}#-!#-A:${data.password}:Xui${data.password.slice(2, 4)}-#`)
    .digest("hex");
}

const IV_LENGTH = 16;

function encryptAES(text) {
 let iv = crypto.randomBytes(IV_LENGTH);
 let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(encryptionKey.ENCRYPTION_KEY), iv);
 let encrypted = cipher.update(text);

 encrypted = Buffer.concat([encrypted, cipher.final()]);

 return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decryptAES(text) {
 let textParts = text.split(':');
 let iv = Buffer.from(textParts.shift(), 'hex');
 let encryptedText = Buffer.from(textParts.join(':'), 'hex');
 let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(encryptionKey.ENCRYPTION_KEY), iv);
 let decrypted = decipher.update(encryptedText);

 decrypted = Buffer.concat([decrypted, decipher.final()]);

 return decrypted.toString();
}

module.exports = {
  encrypt: encrypt,
  decryptAES: decryptAES,
  encryptAES: encryptAES
}