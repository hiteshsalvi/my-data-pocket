const logkat = require('@logkat/logger');
const { Datastore } = require('@google-cloud/datastore');
const datastore = new Datastore();

function createKey(kind, id) {
  if (id) {
    return datastore.key([kind, id]);
  } else {
    return datastore.key([kind]);
  }
}

function getKey(entity) {
  return entity[datastore.KEY]
}

function setKey(key, entity) {
  entity[datastore.KEY] = key;
  return entity;
}

async function save(kind, entity, id) {
  let key = createKey(kind, id);
  let result = await datastore.save({
    key: key,
    data: entity
  });
  logkat.verbose(result, true);
  return key;
}

async function update(entity) {
  await datastore.upsert(entity);
  return entity.key;
}

async function fetchEntity(key){
  return (await datastore.get(key))[0];
}

async function get(kind, fieldName, fieldValue) {
  const query = datastore.createQuery(kind).filter(fieldName, '=', fieldValue);
  return (await datastore.runQuery(query))[0];
}

module.exports = {
  KEY: datastore.KEY,
  createKey: createKey,
  get: get,
  fetchEntity: fetchEntity,
  getKey: getKey,
  setKey: setKey,
  save: save,
  update: update
};
