var { get, KEY, getKey } = require('../../utils/datastore-util');
var { encrypt } = require('../../utils/encryptor');
var usersKind = require('../../configs/env-config.json')[process.env.NODE_ENV ? process.env.NODE_ENV : 'development'].datastoreConfig.usersKind;

async function fetchUsers(email) {
  let results = await get(usersKind, 'email', email);
  console.log('fetchUser');
  console.log(results.length);
  return results;
}

async function getUser(data) {
  let credential = encrypt(data);
  let users = await get(usersKind, 'email', data.email);
  console.log(users.length);
  let userEntity = users.find(user => user.credential === credential);
  if (userEntity){
    userEntity = {
      ...userEntity,
      keyId: userEntity[KEY].id,
      isPasswordMatch: true,
      isError: false
    }
    delete userEntity.credential;
    return userEntity;
  }
  if (users && !userEntity){
    return {isPasswordMatch: false, isError: false};
  } else {
    return {isPasswordMatch: false, isError: true}
  }
}

module.exports = {
  getUser: getUser,
  getKey: getKey,
  fetchUsers: fetchUsers
}