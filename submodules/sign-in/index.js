var express = require('express');
var router = express.Router();
const delay = require('delay');
let { getUser, fetchUsers, getKey } = require('./functions');
let { getUserAuthenticated } = require('../../utils/session-util');
let logger = require('../../utils/logger');

router.get('/', getUserAuthenticated, async (req, res) => {
  if (!req.user){
    res.redirect('/');
  } else if(req.user && req.session.user){
    res.redirect('/home');
  } else {
    console.log(logger);
    logger.log(`during login email -> ${req.user.profile._json.email}`);
    let user = (await fetchUsers(req.user.profile._json.email)).find(user => user.email === req.user.profile._json.email);
    if (user) {
      // check for credential
      if (user.credential && user.isAllowed) {
        res.render('login', {user: req.user.profile._json.email, uI: getKey(user).id});
      } else if (!user.credential && user.isAllowed) {
        logger.log('login function');
        res.redirect(`/register/${getKey(user).id}`);  // add /id
      } else {
        let status = 501;
        res.status(status).render('error', {
          errorCode: status,
          errorMessage: 'My Pocket is a personal service and not open to public.',
          showLogout: req.user ? true : false
        });
      }
    } else {
      // This condition is probably never going to be true
      // disacrding user login since the user who tried to access login page does not exist in datastore
      req.logout();
      res.redirect('/');
    }
  }
});

router.post('/', getUserAuthenticated, async (req, res) => {
  // console.log(req.body);
  // console.log(req.user);
  // console.log(req.session);
  let user = await getUser(req.body);
  logger.log(user);

  // let user = (await fetchUsers(req.user.profile._json.email)).find(user => user.email === req.user.profile._json.email);
  if (user.isError) {
    // logout
    req.logout();
    res.redirect('/');
  } else {
    if (user.isPasswordMatch) {
      req.session.user = user;
      req.session.save(err => {
        logger.error(err);
      });
      await delay(500);
      res.redirect('/home');
    } else {
      res.render('login', {user: req.body.email, uI: '', errorMessage: 'Password Mismatch'});
    }
  }
});

module.exports = router;