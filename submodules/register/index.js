var express = require('express');
var router = express.Router();
let { register, fetchUser } = require('./functions');
let { getUserAuthenticated } = require('../../utils/session-util');

router.get('/:userId?', getUserAuthenticated, async (req, res) => {
  let userId = req.params.userId;
  console.log(`user id -. ${userId}`);
  if(req.session.user){
    res.redirect('/home');
  } else if (userId) {
    let user = await fetchUser(parseInt(userId));
    if (user) {
      console.log('user exists');
      console.log(user);
      if (user.isAllowed && user.credential) {
        res.redirect('/login');
      } else if (user.isAllowed && !user.credential) {
        res.render('register', {uI: userId, user: req.user.profile._json.email});
      } else {
        let status = 401;
        res.status(status).render('error', {
          errorCode:status,
          errorMessage: 'You are not authorised to access',
          showLogout: req.user ? true : false
        });
      }
    } else {
      res.redirect('/');
    }
  } else {
    res.redirect('/');
  }
});

router.post('/', getUserAuthenticated, (req, res) => {
  // resetRequestSessionCookie(req);
  console.log('/register POST');
  console.log(JSON.stringify(req.body, null, 2));
  if (req.session.user){
    res.redirect('/home');
  } else {
    let password = req.body.password;
    let confirmPassword = req.body['confirm-password']
    if (password !== confirmPassword) {
      console.log('if condition');
      res.render('register', {error: 'Password and Confirm password are mismatched.'});
    } else {
      console.log('else');
      register(req.body)
        .then(userEntity => {
          console.log(userEntity);
          res.redirect('/login');
        })
        .catch(error => {
          console.log('An error is generated');
          console.log(error);
          res.render('/error', {error: 'Unknown Error. Kindly contact admin'});
        });
    }
  }
});

module.exports = router;