var { update, createKey, fetchEntity } = require('../../utils/datastore-util');
var { encrypt } = require('../../utils/encryptor')
var configuration = require('../../configs/env-config.json');


async function fetchUser(id,
  kind=configuration[process.env.NODE_ENV ? process.env.NODE_ENV : 'development'].datastoreConfig.usersKind){
let userKey = createKey(kind, id);
  return await fetchEntity(userKey);
}

async function register(data) {
  console.log(configuration);
  let entity = await fetchUser(parseInt(data.uI))

  let credential = encrypt(data);
  entity.credential = credential;
  entity.lastPasswordUpdate = new Date();
  let userEntity = await update(entity);
  return userEntity;
}

module.exports = {
  register: register,
  fetchUser: fetchUser
}