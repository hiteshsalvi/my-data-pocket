var path = require('path');
var express = require('express');
var router = express.Router();
var signInRouter = require('./sign-in');
var signUpRouter = require('./register');
var { get , getKey} = require('../utils/datastore-util');
let { getTwoFactorAuthenticated } = require('../utils/session-util');
const passport = require('passport');
const delay = require('delay');

router.get('/', passport.authenticate('google', {
  scope: ['email'] // Used to specify the required data
}));

router.get(
  '/auth/google/callback',
  passport.authenticate('google'),
  async (req, res) => {
    var configuration = require('../configs/env-config.json')[req.app.get('env') ? req.app.get('env') : 'development'];
    console.log(`email -> ${req.user.profile._json.email}`);
    let users = await get(configuration.datastoreConfig.usersKind, 'email', req.user.profile._json.email || '');
    let user = users.find(user => user.email === req.user.profile._json.email);
    if(user && user.isAllowed && user.credential) {
      res.redirect('/login');
    } else if (user && user.isAllowed && !user.credential){
      res.redirect(`/register/${getKey(user).id}`);
    } else if(user && !user.isAllowed) {
      req.logout();
      let status = 403;
      res.status(status).render('error', {
        errorCode: status,
        errorMessage:'You are not allowed any more. Please contact admin.',
        showLogout: req.user ? true : false
      });
    } else {
      req.logout();
      let status = 401;
      res.status(status).render('error', {
        errorCode: status,
        errorMessage:'Please contact admin to get yourself registered.',
        showLogout: req.user ? true : false
      });
    }
  }
);

router.get('/home', getTwoFactorAuthenticated,(req, res) => {
  console.log(req.user.tokens.accessToken);
  res.render('home', {user: req.user.tokens.accessToken});
});

router.get('/error', (req, res) => {
  res.render('error', {
    errorCode: req.query.errorCode || 500,
    errorMessage: req.query.errorMessage || 'Unknown Error. Kindly contact Admin',
    showLogout: req.user ? true : false
  });
});

router.get(['/favicon.ico'], (req, res) => {
  res.sendFile(path.resolve('public/dist/images/favicon.png'));
});

router.use('/register', signUpRouter);
router.use('/login', signInRouter);

router.get('/logout', async (req, res) => {
  req.logout();
  req.session.destroy();
  await delay(200);
  res.redirect('/');
});

router.get('/_ah/warmup', (req, res) => {
  // Handle your warmup logic. Initiate db connection, etc.
  res.status(200).send('');
});

module.exports = router;
