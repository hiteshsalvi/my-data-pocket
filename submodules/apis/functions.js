var request = require('request');
const promisify = require('util').promisify;
request.postAsync = promisify(request.post);
let configuration = require('../../configs/env-config.json')[
  process.env.NODE_ENV ? process.env.NODE_ENV : 'development'
];
const { encryptAES, decryptAES } = require('../../utils/encryptor');

const { save, get } = require('../../utils/datastore-util');

async function retrieveUser(token) {
  return await request.postAsync({
    headers: { 'content-type': 'application/json' },
    url: `https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${token}`,
    json: true
  });
}

function* decryptor(arr) {
	for(i of arr){
	  yield {...i, k: decryptAES(i.k), v: decryptAES(i.v)};
	}
}

async function saveData(data){
  let result;
  let token = data.q.u;
  let response = {iS: false};
  try{
    result = await retrieveUser(token);
  } catch (error) {
    result = {body: {}};
    console.error(error);
  }
  if (result.body.issued_to === configuration.clientId) {  // Add one more condition of use email. result.body.email == incoming user in the request
    let entity = {
      creator: result.body.email,
      createdAt: new Date(),
      k: encryptAES(data.q.k),
      v: encryptAES(data.q.v)
    }
    response.iS = true;
    await save(configuration.datastoreConfig.passwordsKind, entity);
  } else {
    response.iS = false;
  }
  return response;
}

async function fetchData(data){
  let result;
  let token = data.q.u;
  console.log(`token : ${token}`);
  let response = {iS: false, d: []};
  try{
    result = await retrieveUser(token);
  } catch (error) {
    result = {body: {}};
    console.error(error);
  }
  if (result.body.issued_to === configuration.clientId) {  // Add one more condition of use email. result.body.email == incoming user in the request
    let entities = await get(configuration.datastoreConfig.passwordsKind, 'creator', result.body.email || '')
    response.iS = true;
    response.d = Array.from(decryptor(entities));
  }
  return response
}

module.exports = {
  save: saveData,
  fetch: fetchData
};