const express = require('express');
const router = express.Router();
const { save, fetch } = require('./functions');

router.post('/fetch', (req, res) => {
  console.log(JSON.stringify(req.body));
  fetch(req.body)
    .then(response => {
      res.json(response);
    })
    .catch(error => {
      console.error(error);
      res.status(502).json([]);
    })
});

router.post('/save', (req, res) => {
  console.log(JSON.stringify(req.body));
  save(req.body)
    .then(response => {
      res.json(response);
    })
    .catch(err => {
      console.error(err);
    });
});

module.exports = router;