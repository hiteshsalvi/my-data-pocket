# This Dockerfile meant to increase the speed of CI-CD. The resultant image is being used directly in CI pipeline.

# Repo: hiteshsalavi/alpine-cloud-sdk-node12
# docker build -t hiteshsalavi/alpine-cloud-sdk-node12:1.1 .
# docker push hiteshsalavi/alpine-cloud-sdk-node12:1.1


# Change Log
# 1.0 - It doesn't have virtualenv installed.
# 1.1 - virtualenv installed for python3

FROM google/cloud-sdk:alpine

RUN apk update
RUN apk add nodejs
RUN apk update
RUN apk add npm
RUN pip3 install virtualenv
