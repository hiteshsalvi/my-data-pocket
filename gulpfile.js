const gulp = require('gulp');
const pump = require('pump');
const argv = require('yargs').argv;
const plugins = require('gulp-load-plugins')();
const gulpConfig = require('./configs/gulp/source.json')[argv.dev ? 'dev' : 'prod'];
console.log(argv);
console.log(JSON.stringify(gulpConfig, null, 2));
gulp.task('build-js', cb => {
  if (argv.dev) {
    pump(
      [
        gulp.src(gulpConfig.build.js.files),
        plugins.concat(gulpConfig.build.js.distFile),
        plugins.rename(path => (path.basename += '.min')),
        gulp.dest(gulpConfig.build.js.dist)
      ],
      cb
    );
  } else {
    pump(
      [
        gulp.src(gulpConfig.build.js.files),
        plugins.concat(gulpConfig.build.js.distFile),
        plugins.uglifyEs.default(),
        plugins.rename(path => (path.basename += '.min')),
        gulp.dest(gulpConfig.build.js.dist)
      ],
      cb
    );
  }

  //   return gulp
  //     .src(gulpConfig.build.js.files)
  //     .pipe(plugins.concat(gulpConfig.build.js.distFile))
  //     //.pipe(plugins.uglify())
  //     .on('error', err => {
  //       console.error(err);
  //     })
  //     .pipe(gulp.dest(gulpConfig.build.js.dist));
});

gulp.task('build-css', cb => {
  if (argv.dev) {
    pump(
      [
        gulp.src(gulpConfig.build.css.files),
        plugins.concat(gulpConfig.build.css.distFile),
        plugins.rename(path => (path.basename += '.min')),
        gulp.dest(gulpConfig.build.css.dist)
      ],
      cb
    );
  } else {
    pump(
      [
        gulp.src(gulpConfig.build.css.files),
        plugins.concat(gulpConfig.build.css.distFile),
        // plugins.cleanCss({ debug: true }, details => {
        //   console.log(`${details.name}: ${details.stats.originalSize}`);
        //   console.log(`${details.name}: ${details.stats.minifiedSize}`);
        // }),
        plugins.cleanCss(),
        plugins.rename(path => (path.basename += '.min')),
        gulp.dest(gulpConfig.build.css.dist)
      ],
      cb
    );
  }
});

gulp.task('copy-images', cb => {
  return gulp.src(gulpConfig.copy.images.files, {})
    .pipe(gulp.dest(gulpConfig.copy.images.dist));
});

gulp.task('copy-fonts', cb => {
  return gulp.src(gulpConfig.copy.fonts.files, {})
    .pipe(gulp.dest(gulpConfig.copy.fonts.dist));
});

/*eslint-disable no-console*/
gulp.task('build', gulp.series(['build-js', 'build-css', 'copy-images', 'copy-fonts']), function(done){
  console.log('build task was successful');
});
