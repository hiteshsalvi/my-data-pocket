var createError = require('http-errors');
const resolve = require('path').resolve;
const { Datastore } = require('@google-cloud/datastore');
var express = require('express');
const session = require('express-session');
var cors = require('cors');
// var { get , getKey} = require('./utils/datastore-util');
var cookieParser = require('cookie-parser');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');

const DatastoreStore = require('@google-cloud/connect-datastore')(session);

const envSetup = require('./environment-setup');
envSetup.init();

var indexRouter = require('./submodules');
var apiRouter = require('./submodules/apis');

var logger = require('./utils/logger');
var app = express();
var configuration = require('./configs/env-config.json')[app.get('env') ? app.get('env') : 'development'];

app.use(session({
    store: new DatastoreStore({
      kind: configuration.datastoreConfig.sessionStore,
      dataset: new Datastore(),
    }),
    cookie: {
      maxAge: configuration.sessionTimeOut,
      httpOnly: true,
      secure: false,
      // secure: app.get('env') ? true : false
    },
    rolling: true,
    // saveUninitialized: app.get('env') ? true : false,
    saveUninitialized: false,
    resave: false,
    secret: configuration.secret,
  })
);
app.use(cookieParser(configuration.secret));

app.set('views', resolve('./views'));
app.set('view engine', 'pug');
app.use(express.static(resolve('./public')));

app.use(passport.initialize()); // Used to initialize passport
app.use(passport.session()); // Used to persist login sessions

passport.use(
  new GoogleStrategy(
    {
      clientID: configuration.clientId,
      clientSecret: configuration.clientSecret,
      callbackURL: '/auth/google/callback',
    },
    (accessToken, refreshToken, profile, done) => {
      done(null, {tokens: {accessToken: accessToken, refreshToken: refreshToken}, profile: profile});
    }
  )
);

// app.get(
//   '/auth/google/callback',
//   passport.authenticate('google'),
//   async (req, res) => {
//     console.log(`email -> ${req.user.profile._json.email}`);
//     console.log(`${req.path}`);
//     let users = await get(configuration.datastoreConfig.usersKind, 'email', req.user.profile._json.email || '');
//     let user = users.find(user => user.email === req.user.profile._json.email);
//     if(user && user.isAllowed && user.credential) {
//       res.redirect('/login');
//     } else if (user && user.isAllowed && !user.credential){
//       res.redirect(`/register/${getKey(user).id}`);
//     } else if(user && !user.isAllowed) {
//       req.logout();
//       let status = 403;
//       res.status(status).render('error', {
//         errorCode: status,
//         errorMessage:'You are not allowed any more. Please contact admin.',
//         showLogout: req.user ? true : false
//       });
//     } else {
//       req.logout();
//       let status = 401;
//       res.status(status).render('error', {
//         errorCode: status,
//         errorMessage:'Please contact admin to get yourself registered.',
//         showLogout: req.user ? true : false
//       });
//     }
//   }
// );

passport.serializeUser((user, done) => {
  done(null, user);
});

// Used to decode the received cookie and persist session
passport.deserializeUser((user, done) => {
  done(null, user);
});

// var corsOptions = {
//     origin: 'https://n-drqkd447cxjm3xxs3lvuowb75hq6coxg66obefi-0lu-script.googleusercontent.com',
//     optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// };
app.use(cors());

app.options('*', cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/apis', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  logger.log(req.user);
  if (req.user) {
    res.status(err.status || 500);
    res.render('error', {errorCode: 404, errorMessage: 'Page Not Found', showLogout: req.user ? true : false});
  } else {
    res.redirect('/');
  }
});

module.exports = app;
