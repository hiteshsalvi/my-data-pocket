// angular.module('myPocketApp', ['ngClipboard'])
myPocketApp.controller('h', [
  'ngClipboard',
  '$scope',
  '$http',
  async function (ngClipboard, $scope, $http) {
    $scope.d = {}; // data
    $scope.fR = []; // filteredRecords
    $scope.r = []; // Records

    async function rU(token) {
      return new Promise((resolve, reject) => {
        $http
          .post(
            `https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${token}`
          )
          .then((response) => {
            resolve(response.data || {});
          })
          .catch((error) => {
            reject(error);
          });
      });
    }
    function eD(data, resp) {
      // console.log('eD');
      // console.log(data);
      data = {
        u: data.u,
        k: CryptoJS.AES.encrypt(data.k, data.p).toString(),
        v: CryptoJS.AES.encrypt(data.v, data.p).toString(),
      };
      // console.log(data);
      return data;
    }

    function dE(e, p) {
      // console.log('dE');
      // console.log(e);
      return CryptoJS.AES.decrypt(e, p).toString(CryptoJS.enc.Utf8);
    }

    function eE(e, p) {
      return CryptoJS.AES.encrypt(e, p).toString();
    }

    function* dR(arr) {
      for (i of arr) {
        // console.log(`i.k -> ${i.k}`);
        // console.log(`password : -> ${$scope.d.p}`);
        // console.log(dE(i.k, $scope.d.p));
        yield { k: dE(i.k, $scope.d.p), v: i.v };
      }
    }

    function dynamicSort(property) {
      let sortOrder = 1;

      if (property[0] === '-') {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function (a, b) {
        if (sortOrder == -1) {
          return b[property].localeCompare(a[property]);
        } else {
          return a[property].localeCompare(b[property]);
        }
      };
    }
    $scope.sv = async function () {
      // console.log('Save function called');
      let res = await rU($scope.d.u);
      if (
        res.issued_to ===
        '153983034287-hdci2uc0i09uk86h53e51okkj4eflndd.apps.googleusercontent.com'
      ) {
        // console.log($scope.d);
        let encryptedData = eD($scope.d, res);
        // console.log('Encrypted Data');
        // console.log(encryptedData);
        $scope.r = [];
        $http
          .post('/apis/save', { q: encryptedData })
          .then(function (response) {
            // console.log(response);
            $scope.d.v = '';
            $scope.d.k = '';
            $scope.fetch(true);
          })
          .catch(function (error) {
            // console.error(error);
          });
      }
    };

    $scope.fetch = function (fF = false) {
      // console.log('Fetch Function called');
      if (fF || !$scope.r.length) {
        // console.log('F');
        $http
          .post('/apis/fetch', { q: $scope.d })
          .then(function (response) {
            // console.log(response.data.d);
            $scope.r = Array.from(dR(response.data.d));
            // console.log($scope.r);
            // $scope.r = response.data.d;
          })
          .catch(function (error) {
            $scope.r = response.data.d;
            // console.error(error);
          });
      }
    };

    $scope.view = function (e) {
      // View
      e.vi = !e.vi;
      e.v = e.vi ? dE(e.v, $scope.d.p) : eE(e.v, $scope.d.p);
      if (e.vi) {
        ngClipboard.toClipboard(e.v);
      }
      // console.log(e);
      // console.log('hereBy completing view Op');
    };

    $scope.s = function (q) {
      // search
      if (q) {
        $scope.fR = ($scope.r || [])
          .filter((e) => e.k.toLowerCase().includes(q.toLowerCase()))
          .sort(dynamicSort('k'));
        // console.log($scope.fR);
      } else {
        $scope.fR = [];
      }
    };
  },
]);
