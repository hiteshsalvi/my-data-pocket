const resolve = require('path').resolve;
const logkat = require('@logkat/logger');

function init() {
  if (process.env.NODE_ENV === 'production') {
    require('@google-cloud/trace-agent').start();
    require('@google-cloud/debug-agent').start();
    logkat.setLogLevel('debug');
  } else if (process.env.NODE_ENV === 'development') {
  } else if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
    process.env.GOOGLE_APPLICATION_CREDENTIALS = resolve(
      './configs/keys/editor.json'
    );
    logkat.setLogLevel('verbose');
  }
}

module.exports = {
  init: init
};
